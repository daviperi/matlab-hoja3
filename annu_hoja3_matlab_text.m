%% Practicas de Matlab
%% Resolución de EDO con métodos monopaso
%% Hoja 3
% *Nombre: David*
% 
% *Apellido: Periañez Rollán*
% 
% *DNI: 05333330K*
%% 
% 
%% 1. Implementación de métodos explícitos
% Práctica 1 (Implementación del método de Euler explícito) 
% Escribir en el Apéndice A1 una función implementando el método de Euler (explícito) 
% 
% $$      \left\{\begin{array}{l}               y_{i+1}=y_i + h f(t_i,y_i) \quad 
% i=0,\ldots ,N-1          \\               y_0 \approx a        \end{array}$$
% 
% para el PVI (problema de valor inicial para sistemas de EDOs) y que responda 
% a la sintaxis
% 
% |[t,y]=mieuler(f,intv,y0,N)|
% 
% El pseudocódigo correspondiente se encuentra en el CV (campus virtual). 
% Práctica 2 (Implementación del método de Euler modificado explícito) 
% Escribir en el Apéndice A1 una función que implemente el método de Euler modificado 
% (explícito) 
% 
% $$\begin{array}{ccl}  y_{i+1} &=& y_i + h f\left(t_i + \frac{h}{2}, y_i + 
% \frac{h}{2} f(t_i,y_i)\right), \quad  i=0,\ldots ,N-1 \\  y_0 &\approx& a\end{array}$$
% 
% para el PVI (problema de valor inicial para sistemas de EDOs) y que responda 
% a la sintaxis
% 
% |[t,y]=mieulermod(f,intv,y0,N)| 
% Práctica 3 (Implementación del método de Euler mejorado explícito) 
% Escribir en el Apéndice A1 una función que implemente el método de Euler mejorado 
% (explícito) 
% 
% $$\begin{array}{ccl}y_{i+1} &=& y_i +  \left.{h\over 2} (f(t_i,y_i) + f(t_{i+1},  
% y_i+hf(t_i,y_i)\right), \quad i=0,\ldots ,N-1\\ y_0 &\approx& a\end{array}$$
% 
% para el PVI (problema de valor inicial para sistemas de EDOs) y que responda 
% a la sintaxis
% 
% |[t,y]=mieulermej(f,intv,y0,N)| 
% Práctica 4 (Implementación del método de Runge-Kutta explícito) 
% Escribir en el Apéndice A1 una función que implemente el método de Euler mejorado 
% (explícito) 
% 
% $$    \begin{array}{ccl}      y_{i+1} &=& y_i + h \Phi(t_i,y_i,h), \quad i=0,\ldots 
% ,N-1 \\      y_0 &\approx& a    \end{array}$$
% 
% donde $\Phi(t,y,h)=\frac{1}{6}\left(F_1+2F_2+2F_3+F_4\right)$ y 
% 
% $$    \begin{array}{l}      F_1=f(t,y)\\      F_2=f\left(t+\frac{h}{2},y+\frac{h}{2}F_1\right)\\      
% F_3=f\left(t+\frac{h}{2},y+\frac{h}{2}F_2\right) \\      F_4=f\left(t+h,y+hF_3\right),    
% \end{array}$$
% 
% para el PVI (problema de valor inicial para sistemas de EDOs) y que responda 
% a la sintaxis
% 
% |[t,y]=mirk4(f,intv,y0,N)|
% Práctica 5 (EDO de corazón) 
% Considera el siguiente PVI
% 
% $$    \begin{array}{ccc}    \frac{dx_1}{dt} & = & x_2                 \\    
% \frac{dx_2}{dt} & = & -16x_1 + 4 \sin(2t) \\    x_1(0)          & = & 0                  
% \\    x_2(0)          & = & 2    \end{array}$$
% 
% en el intervalo, $[0,2 \pi]$.  Ahora intenta resolverla numéricamente usando
%% 
% # el método de Euler $N=100,400,800$
% # el método de Euler modificado
% # el método de Euler mejorado 
% # el método de Runge Kutta 4 
%% 
% pinta el diagrama de fases.
% 
% *Solución*
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 

disp('Hoja 3: Ej 1 David Peri');
[y, t] = mieuler(f, intv, y0, N);    
%%
disp('Hoja 3: Ej 2 David Peri');
[y, t] = mieulermod(f, intv, y0, N);
%%
disp('Hoja 3: Ej 3 David Periañez');
[y, t] = mieulermej(f, intv, y0, N);
%%
disp('Hoja 3: Ej 4 David Peri');
[y, t] = mirk4(f, intv, y0, N);
%%
disp('Hoja 3: Ej 5 David Peri');
%Datos iniciales:
y0 = [0; 2];
intv = [0, 2*pi];
N = [100, 400, 800];
h = (intv(end)-intv(1))./N;
f = @(t, y)[y(2); -16*y(1) + 4*sin(2*t)];

%Con N=100
[t, y] = mieuler(f, intv, y0, N(1));
%representa el plano de fases y1, y2:
plot(y(1, :), y(2, :));
title("Diagrama de Fase (Corazón); Método Euler; h = " + h(1) + ...
    "; y0 = [0, 2]; N = " + N(1));
disp(y(:, end));

%Con N=400
[t, y] = mieuler(f, intv, y0, N(2));
plot(y(1, :), y(2, :));
title("Diagrama de Fase (Corazón); Método Euler; h = " + h(2) + ...
    "; y0 = [0, 2]; N = " + N(2));
disp(y(:, end));

%Con N=800
[t, y] = mieuler(f, intv, y0, N(3));
plot(y(1, :), y(2, :));
title("Diagrama de Fase (Corazón); Método Euler; h = " + h(3) + ...
    "; y0 = [0, 2]; N = " + N(3));
disp(y(:, end));

%Euler Modificado
[t, y] = mieulermod(f, intv, y0, N(1));
plot(y(1, :), y(2, :));
title("Diagrama de Fase (Corazón); Método Euler Mod; h = " + h(1) + ...
    "; y0 = [0, 2]; N = " + N(1));
disp(y(:, end));

%Euler Mejorado
[t, y] = mieulermej(f, intv, y0, N(1));
plot(y(1, :), y(2, :));
title("Diagrama de Fase (Corazón); Método Euler Mej; h = " + h(1) + ...
    "; y0 = [0, 2]; N = " + N(1));
disp(y(:, end));

%Runge-Kutta 4
[t, y] = mirk4(f, intv, y0, N(1));
plot(y(1, :), y(2, :));
title("Diagrama de Fase (Corazón); Método RK4; h = " + h(1) + ...
    "; y0 = [0, 2]; N = " + N(1));
disp(y(:, end));

%% Apéndice código: funciones de Euler, Euler modificado, Euler mejorado y Runge-Kutta 4
%% 
% Función del método de Euler (básico):

function [t, y] = mieuler(f, intv, y0, N)
    t = intv(1);
    y = y0;
    h = (intv(end) - intv(1))/N;
    %Inicializa Var.aux.  que recogen el valor de la última iteración.
    t_aux = t;
    y_aux = y;
    for k = 1:N
        %Actualiza las variables auxiliares (Euler).
        y_aux = y_aux + h*f(t_aux, y_aux);
        t_aux = t_aux + h;
        %Añade los nuevos valores a las tuplas t, y.
        t = [t, t_aux];
        y = [y, y_aux];
    end
end
%% 
% Función del método de Euler modificado:

function [t, y] = mieulermod(f, intv, y0, N)
    t = intv(1);
    y = y0;
    h = (intv(end) - intv(1))/N;
    %Inicializa Var.aux.  que recogen el valor de la última iteración.
    t_aux = t;
    y_aux = y;
    for k = 1:N
        %Actualiza las variables auxiliares (Euler).
        y_aux = y_aux + h*f(t_aux + h/2, y_aux + h/2*f(t_aux, y_aux));
        t_aux = t_aux + h;
        %Añade los nuevos valores a las tuplas t, y.
        t = [t, t_aux];
        y = [y, y_aux];
    end
end
%% 
% Función del método de Euler mejorado:

function [t, y] = mieulermej(f, intv, y0, N)
    t = intv(1);
    y = y0;
    h = (intv(end) - intv(1))/N;
    %Inicializa Var.aux.  que recogen el valor de la última iteración.
    t_aux = t;
    y_aux = y;
    for k = 1:N
        %Actualiza las variables auxiliares (Euler).
        y_aux = y_aux + h/2*(f(t_aux, y_aux) + f(t_aux + h, y_aux + h*f(t_aux, y_aux)));
        t_aux = t_aux + h;
        %Añade los nuevos valores a las tuplas t, y.
        t = [t, t_aux];
        y = [y, y_aux];
    end
end
%% 
% Función del método RK4:

function [t, y] = mirk4(f, intv, y0, N)
    t = intv(1);
    y = y0;
    h = (intv(end) - intv(1))/N;
    %Inicializa Var.aux.  que recogen el valor de la última iteración.
    t_aux = t;
    y_aux = y;
    for k = 1:N
        %F1 a F4 construyen la función phi.
        F1 = f(t_aux, y_aux);
        F2 = f(t_aux + h/2, y_aux + h/2*F1);
        F3 = f(t_aux + h/2, y_aux + h/2*F2);
        F4 = f(t_aux + h, y_aux + h*F3);
        %La matriz y se actualiza usando la phi.
        phi = 1/6*(F1 + 2*F2 + 2*F3 + F4);
        y_aux = y_aux + h*phi;
        t_aux = t_aux + h;
        t = [t, t_aux];
        y = [y, y_aux];
    end
end